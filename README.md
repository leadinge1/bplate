# Batch Template for 2018/12合宿

## Overview
- 開発合宿で、pythonでバッチプログラムを製造する際のリファレンスとして、batchテンプレートを説明する。

## Contents

### Tools Installation

- 以下を参考に、Anaconda,Git(Sourcetree)をインストールください。
    - Anaconda site
    - https://www.anaconda.com/
        + installation
        + https://docs.anaconda.com/anaconda/install/windows/
        + download
        + https://www.anaconda.com/download/

    - Sourcetree
    - https://www.sourcetreeapp.com/
        + git for windows
        + https://git-scm.com/download/win
            - git commandにpathが通っていることを確認ください。
        
```
G:\Users\sakai>git --version
git version 2.12.0.windows.1
        
G:\Users\sakai>whereis git
C:\Program Files\Git\cmd\git.exe
```

### Batch Template環境作成とinstallation

1. Anaconda promptを起動
2. `conda create -n xxxxx`で、python環境を生成
3. `git clone https://leadinge1@bitbucket.org/<target repogitory>` でClone
4. `pip install -r requirements.txt`でpython moduleをインストール
5. `jupyter notebook README.ipynb`を起動

#### １．Anaconda promptを起動
1. Win Key
2. "anaconda prompt"と入力
3. anaconda promptを起動
4. conda を確認

```
(base) C:\Users\sakai>conda --version
conda 4.5.11

(base) C:\Users\sakai>pip --version
pip 10.0.1 from G:\Anaconda3\lib\site-packages\pip (python 3.7)

(base) C:\Users\sakai>python --version
Python 3.7.0

(base) C:\Users\sakai>conda -h
usage: conda [-h] [-V] command ...

conda is a tool for managing and deploying applications, environments and packages.

Options:

positional arguments:
  command
    clean        Remove unused packages and caches.
    config       Modify configuration values in .condarc. This is modeled
                 after the git config command. Writes to the user .condarc
                 file (G:\Users\sakai\.condarc) by default.
    create       Create a new conda environment from a list of specified
                 packages.
    help         Displays a list of available conda commands and their help
                 strings.
    info         Display information about current conda install.
    install      Installs a list of packages into a specified conda
                 environment.
    list         List linked packages in a conda environment.
    package      Low-level conda package utility. (EXPERIMENTAL)
    remove       Remove a list of packages from a specified conda environment.
    uninstall    Alias for conda remove. See conda remove --help.
    search       Search for packages and display associated information. The
                 input is a MatchSpec, a query language for conda packages.
                 See examples below.
    update       Updates conda packages to the latest compatible version. This
                 command accepts a list of package names and updates them to
                 the latest versions that are compatible with all other
                 packages in the environment. Conda attempts to install the
                 newest versions of the requested packages. To accomplish
                 this, it may update some packages that are already installed,
                 or install additional packages. To prevent existing packages
                 from updating, use the --no-update-deps option. This may
                 force conda to install older versions of the requested
                 packages, and it does not prevent additional dependency
                 packages from being installed. If you wish to skip dependency
                 checking altogether, use the '--force' option. This may
                 result in an environment with incompatible packages, so this
                 option must be used with great caution.
    upgrade      Alias for conda update. See conda update --help.

optional arguments:
  -h, --help     Show this help message and exit.
  -V, --version  Show the conda version number and exit.

conda commands available from other packages:
  build
  convert
  develop
  env
  index
  inspect
  metapackage
  render
  server
  skeleton

(base) C:\Users\sakai>conda info -e
# conda environments:
#
base                  *  G:\Anaconda3


(base) C:\Users\sakai>
```

#### 2. `conda create -n xxxxx`で、python環境を生成

- １．作業フォルダに移動

```
(base) C:\Users\sakai>g:

(base) G:\>cd workspace\py
```

- ２．batch用のpython環境を生成

```
(base) G:\workspace\py>conda create -n batch python=3.7
 :
(base) G:\workspace\py>conda activate batch

(batch) G:\workspace\py>pip freeze
certifi==2018.10.15
wincertstore==0.2

(batch) G:\workspace\py>python
Python 3.7.1 (default, Oct 28 2018, 08:39:03) [MSC v.1912 64 bit (AMD64)] :: Anaconda, Inc. on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> import sys
>>> for p in sys.path:
...     print(p)
...

G:\Anaconda3\envs\batch\python37.zip
G:\Anaconda3\envs\batch\DLLs
G:\Anaconda3\envs\batch\lib
G:\Anaconda3\envs\batch
G:\Anaconda3\envs\batch\lib\site-packages
>>> quit()
```

#### 3. `git clone https://leadinge1@bitbucket.org/<target repogitory>` でClone

- `git clone https://leadinge1@bitbucket.org/leadinge1/bplate.git`でClone

```
(batch) G:\workspace\py>git --version
git version 2.12.0.windows.1

(batch) G:\workspace\py>git --help
usage: git [--version] [--help] [-C <path>] [-c name=value]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p | --paginate | --no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

These are common Git commands used in various situations:

start a working area (see also: git help tutorial)
   clone      Clone a repository into a new directory
   init       Create an empty Git repository or reinitialize an existing one

work on the current change (see also: git help everyday)
   add        Add file contents to the index
   mv         Move or rename a file, a directory, or a symlink
   reset      Reset current HEAD to the specified state
   rm         Remove files from the working tree and from the index

examine the history and state (see also: git help revisions)
   bisect     Use binary search to find the commit that introduced a bug
   grep       Print lines matching a pattern
   log        Show commit logs
   show       Show various types of objects
   status     Show the working tree status

grow, mark and tweak your common history
   branch     List, create, or delete branches
   checkout   Switch branches or restore working tree files
   commit     Record changes to the repository
   diff       Show changes between commits, commit and working tree, etc
   merge      Join two or more development histories together
   rebase     Reapply commits on top of another base tip
   tag        Create, list, delete or verify a tag object signed with GPG

collaborate (see also: git help workflows)
   fetch      Download objects and refs from another repository
   pull       Fetch from and integrate with another repository or a local branch
   push       Update remote refs along with associated objects

'git help -a' and 'git help -g' list available subcommands and some
concept guides. See 'git help <command>' or 'git help <concept>'
to read about a specific subcommand or concept.


(batch) G:\workspace\py>git clone https://leadinge1@bitbucket.org/leadinge1/bplate.git
Cloning into 'bplate'...
remote: Counting objects: 21, done.
remote: Compressing objects: 100% (19/19), done.
remote: Total 21 (delta 4), reused 0 (delta 0)
Unpacking objects: 100% (21/21), done.
```

#### 4. `pip install -r requirements.txt`でpython moduleをインストール

- `pip install -r requirements.txt`でpython moduleをインストール

```
(batch) G:\workspace\py>cd bplate
(batch) G:\workspace\py\bplate>pip install -r requirements.txt

 :
 :

(batch) G:\workspace\py\bplate>pip freeze

backcall==0.1.0
bleach==3.0.2
Cerberus==1.2
certifi==2018.10.15
colorama==0.4.0
decorator==4.3.0
defusedxml==0.5.0
docopt==0.6.2
entrypoints==0.2.3
ipykernel==5.1.0
ipython==7.1.1
ipython-genutils==0.2.0
ipywidgets==7.4.2
jedi==0.13.1
Jinja2==2.10
jsonschema==2.6.0
jupyter==1.0.0
jupyter-client==5.2.3
jupyter-console==6.0.0
jupyter-core==4.4.0
MarkupSafe==1.1.0
mistune==0.8.4
nbconvert==5.4.0
nbformat==4.4.0
notebook==5.7.0
pandocfilters==1.4.2
parso==0.3.1
pickleshare==0.7.5
prometheus-client==0.4.2
prompt-toolkit==2.0.7
Pygments==2.2.0
python-dateutil==2.7.5
pywinpty==0.5.4
pyzmq==17.1.2
qtconsole==4.4.3
Send2Trash==1.5.0
six==1.11.0
terminado==0.8.1
testpath==0.4.2
tornado==5.1.1
traitlets==4.3.2
wcwidth==0.1.7
webencodings==0.5.1
widgetsnbextension==3.4.2
wincertstore==0.2
```

#### 5. `jupyter notebook README.ipynb`を起動

- `jupyter notebook README.ipynb`を起動

```
(batch) G:\workspace\py\bplate>dir
 Volume in drive G is Users
 Volume Serial Number is 04A1-7D2F

 Directory of G:\workspace\py\bplate

11/13/2018  10:42 PM    <DIR>          .
11/13/2018  10:42 PM    <DIR>          ..
11/13/2018  10:42 PM             1,533 .gitignore
11/13/2018  10:42 PM             3,398 app.py
11/13/2018  10:42 PM             1,441 configbase.py
11/13/2018  10:42 PM    <DIR>          configs
11/13/2018  10:42 PM               628 logging_debug.conf
11/13/2018  10:42 PM             2,932 main - Copy.py
11/13/2018  10:42 PM             2,220 main.py
11/13/2018  10:42 PM    <DIR>          notebook
11/13/2018  10:42 PM             4,364 README.ipynb
11/13/2018  10:59 PM             8,775 README.md
11/13/2018  10:48 PM                37 requirements.txt
               9 File(s)         25,328 bytes
               4 Dir(s)  66,279,223,296 bytes free

(batch) G:\workspace\py\bplate>jupyter notebook README.ipynb
```

### Wrap up

- Anaconda promptを起動して、`conda create -n xxxxx`で、python環境を生成した。
- bplate repositoryより、`git clone https://leadinge1@bitbucket.org/<target repogitory>` でcloneした。
- `pip install -r requirements.txt`により、必要なpython moduleをインストールした。
- `jupyter notebook README.ipynb`で、READMEのjupyter notebookを表示した。

[EOF]
