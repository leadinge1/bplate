#!/usr/bin/env python
"""{fname}
# main process
- read csv file and transform them into json format.
- move files from target folder to backup folder.
"""
__doc__ = __doc__.format(fname=__file__)

import logging.config
from pathlib import Path
import shutil
import pandas as pd
import os
import json
from datetime import datetime

import configbase as base
CONF = base.CONF
ENV = base.ENV

#  -----------------------------------------  #
# local method
# 
def _get_file_list(target_dir,pattern):
    p = Path(target_dir).glob(pattern)
    lst = [fle for fle in p if fle.is_file()]
    return lst

def _move_files(move_to_dir,file_list):
    logger = logging.getLogger(__name__)
    p_move = Path(move_to_dir)
    for path_src in file_list:
        basename = Path(path_src).name
        path_move = p_move / basename
        shutil.move(path_src, path_move)

def _get_timestamp_str():
    dtm = datetime.now()
    return dtm.strftime(CONF.TIME_FORMAT)
    

def exec_proc(target_dir, output_dir, move_to_dir):

    logger = logging.getLogger(__name__)
    try:
        ## variables setting
        glob_pattern = CONF.TARGET_FILE_PTN
        input_flag = CONF.INPUT_FLAG
        data_name  = CONF.DATA_NAME
        data_type = CONF.DATA_TYPE[data_name]
        output_columns = CONF.OUTPUT_COLUMNS[data_name]
        extent_name = CONF.OUPUT_FLAG
        p_output = Path(output_dir)
        timestamp_str = _get_timestamp_str()
        logger.debug(timestamp_str)
        
        ## get target files
        file_list = _get_file_list(target_dir,glob_pattern)
        logger.debug(f'file_list = "{file_list}"')
        
        ## processing loop
        for f in file_list:
            base_name = os.path.basename(str(f)).split('.')[0]
            logger.debug(base_name)
            logger.debug(f'//  Begin   "{base_name}" transform process ----------------------------- //' )
            
            ## generate output file name
            output_file_name = base_name + '_' + timestamp_str + "." + extent_name
            output_file_path = p_output / output_file_name
            logger.debug(output_file_path)
            
            ## read input file and extract
            df = pd.read_csv(f, dtype=data_type, encoding='SHIFT-JIS')
            df_extracted = df.loc[:,output_columns]
            
            ## transform
            jsn = df_extracted.to_json(orient='records',force_ascii=False)
            jsn_out = {data_name : jsn}
            
            ## load output file
            with open(str(output_file_path), mode='w') as fo:
                json.dump(jsn_out,fo,indent=4, ensure_ascii=False)
            
            logger.debug(f'//  Finish  "{base_name}" transform process ----------------------------- //' )
            
        
        # move files
        #_move_files(move_to_dir,file_list)

    except Exception as ex:
        raise(ex)

#  -----------------------------------------  #
# main process
# 
def process(args):
    logger = logging.getLogger(__name__)
    logger.info(">> Begin  main  ------------------------- >>")
    logger.debug(__doc__)
    target_dir = args['target_dir']
    output_dir = args['output_dir']
    move_to_dir = args['move_to_dir']
    logger.debug(f'target_dir = "{target_dir}"')
    logger.debug(f'output_dir = "{output_dir}"')
    logger.debug(f'move_to_dir = "{move_to_dir}"')

    try:
        # execute proess
        exec_proc(target_dir, output_dir, move_to_dir)
        
        logger.info(">> Finish main  ------------------------- <<")

    except Exception as ex:
        raise(ex)

if __name__ == '__main__' :
    logging.config.fileConfig('logging_debug.conf')
    args = {'target_dir' : "target", 'move_to_dir' : "backup", "output_dir" : "output"}
    process(args)
