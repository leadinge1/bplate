#! /usr/bin/env python
"""{fname}
# Project name : {project_name}
## Apps for execution of main.py

Usage:
  {fname} -h | --help
  {fname} --version
  {fname} run [--input=<targetfolder> | --output=<outputfolder> |--backup=<backupfolder>]

Options:
  -h --help     Show this screen.
  --version     Show version.
  run  [--input=<targetfolder> | --output=<outputfolder> |--backup=<backupfolder>]
"""
__author__ = 'Sakai'
__version__ = '0.0.1'
__date__ = '2018-11-11'

import sys
import os

from docopt import docopt
import logging.config

import configbase as base
LOG_CONF = base.LOG_CONF
ENV      = base.ENV
CONF     = base.CONF

__doc__ = __doc__.format(fname=os.path.basename(__file__),project_name=CONF.PROJECT_NAME)

from cerberus import Validator
from jinja2 import Template
from pathlib import Path

import main as batch

#  -----------------------------------------  #
# local methods
def get_template(str,param):
    t = Template(str)
    return t.render(param)

def _get_environ(env_code):
    rtn = os.getenv(env_code,CONF.DEFAULT_ENV_VALUE[env_code] )
    return rtn

def _generate_folder(dir_path,child_path=None):
    p = Path(dir_path)
    if child_path is None:
        built_p = p
    else:
        built_p = p / child_path
    try:
        built_p.mkdir(parents=True, exist_ok=True)
        return built_p
    except:
        raise ValueError(f'is not directory in {dir_path}')

def _exist_default_path(path):
    apps_dir = _get_environ('APPS_DATA')
    gen_dir = _generate_folder(apps_dir,path)
    return str(gen_dir)

# definition for validation
def _set_default(arg):
    if arg == "--input":
        return _exist_default_path(CONF.TARGET_DIR)
    if arg == "--output":
        return _exist_default_path(CONF.OUTPUT_DIR)
    if arg == "--backup":
        return _exist_default_path(CONF.MOVE_TO_DIR)

schema = {
    'run': {
        'type': 'boolean',
        'required': True,
    },
    '--input': {
        'type': 'string',
    },
    '--output': {
        'type': 'string',
    },
    '--backup': {
        'type': 'string',
    },
    '--version': {},
    '--help': {},
}

#  -----------------------------------------  #
# main process
# 
def process():
    """# main process
    """
    logger = logging.getLogger(__name__)
    args = docopt(__doc__)
    logger.debug("\n"+str(args))
    v = Validator(schema)
    
    if args["--version"]:
        print(f'version : {__version__}_{__date__}')
        sys.exit()
    
    if args["run"]:
        try:
            #if v.validate(args):
            if v.normalized(args):
                logger.info("//  Start  app ----------------------------- @@")
                target_dir = _set_default("--input") if (args["--input"] is None )  else args["--input"]
                output_dir = _set_default("--output") if (args["--output"] is None )  else args["--output"]
                move_to_dir = _set_default("--backup") if (args["--backup"] is None )  else args["--backup"]
                _generate_folder(target_dir)
                _generate_folder(output_dir)
                _generate_folder(move_to_dir)
                params = {'target_dir' : target_dir, 'output_dir': output_dir ,'move_to_dir' : move_to_dir}
                logger.debug(params)
                batch.process(params)
                logger.info("//  End    app ----------------------------- @@")
            
            else:
                msg = v.errors
                raise ValueError(msg)
        
        except Exception as ex:
            logger.exception(ex)
            sys.exit(1)

if __name__ == '__main__':
    logging.config.fileConfig(LOG_CONF)
    process()
