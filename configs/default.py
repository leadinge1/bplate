PROJECT_NAME = "Execute batch sample application"

TARGET_DIR = "target"
OUTPUT_DIR = "output"
MOVE_TO_DIR = "backup"

TARGET_FILE_PTN = "**/*.csv"
INPUT_FLAG = 'csv'
OUPUT_FLAG = 'json'

DATA_NAME = 'postcode'

DATA_TYPE = {
    'postcode' : {
        "JISX0401"   : str,
        "POST_CODE5" : str,
        "POST_CODE"  : str,
        "PREFECTURE_KANA": str,
        "CITY_KANA"  : str,
        "TOWN_KANA"  : str,
        "PREFECTURE" : str,
        "CITY"       : str,
        "TOWN"       : str,
        "FLAG_1"     : str,
        "FLAG_2"     : str,
        "FLAG_3"     : str,
        "FLAG_4"     : str,
        "FLAG_5"     : str,
        "REASON_FOR_UPDATE": str,
    },
}

OUTPUT_COLUMNS = {
    'postcode' : ["POST_CODE","PREFECTURE","CITY","TOWN"],
}

TIME_FORMAT = '%Y%m%d_%H%M%S_%f'

DEFAULT_ENV_VALUE = {
    'APPS_DATA' : '.',
    'EXECUTION_MODE' : 'debug'
}

